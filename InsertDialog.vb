﻿Imports System.Windows.Forms

Public Class InsertDialog
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub InsertDialog_Activated(sender As System.Object, e As System.EventArgs) Handles MyBase.Activated
        Nummer_EditBox.Focus()
    End Sub
End Class
