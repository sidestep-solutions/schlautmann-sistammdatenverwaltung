﻿Imports System.Reflection
Imports System.Drawing.Text

Public Class frmInfo
    Dim pi As New ProgrammInfo
    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click

        Me.Dispose()

    End Sub

    Private Sub frmInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.txtVersion.Text = pi.VersionNumber
        Me.txtSideStepInfo.Text = "Copyright (c) 2016" & vbNewLine & "SideStep Business Solutions GmbH" & vbNewLine & "Rathenaustraße 96" & vbNewLine & "33102 Paderborn"
        Me.txtSystemschluessel.Text = GetSystemKey()

    End Sub

    Private Sub UiTabPage1_Click(sender As System.Object, e As System.EventArgs) Handles UiTabPage1.Click

    End Sub

    Private Sub UiTabPage1_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles UiTabPage1.Paint
        Dim fontFamily As New FontFamily("Arial")
        Dim fontSize As Integer = 29
        Dim font As New Font( _
           fontFamily, _
           fontSize, _
           FontStyle.Bold, _
           GraphicsUnit.Pixel)
        Dim solidBrush As New SolidBrush(Color.FromArgb(255, 90, 90, 90))
        Dim string1 As String = pi.ProductName

        e.Graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit
        e.Graphics.DrawString(string1, font, solidBrush, New PointF(0, 10))
    End Sub
End Class