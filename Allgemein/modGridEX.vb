﻿Imports Janus.Windows.GridEX
Imports System.Data.SqlClient

Public Module modGridEX

    Public Function FormatGrid(myGridEX As GridEX, myGridName As String) As Janus.Windows.GridEX.GridEX

        Try

            Dim iPos As Integer

            For i = 0 To myGridEX.RootTable.Columns.Count - 1
                myGridEX.RootTable.Columns(i).Visible = False
            Next


            iPos = 0

            myGridEX.ColumnAutoResize = True
            myGridEX.VisualStyle = VisualStyle.Office2007


            If myGridName = "GridEXNotification" Or _
                myGridName = "GridEXNotificationClient" Then
                myGridEX = FormatGrid_Notification(myGridEX, myGridName)
            End If


            If myGridName = "GridEXProtokoll" Then
                myGridEX = FormatGrid_Protokoll(myGridEX, myGridName)
            End If

            If myGridName = "GridEXEingangspostPartner" Or _
                myGridName = "GridEXEingangspostTyp" Or _
                 myGridName = "GridEXBarcodes" Then

                myGridEX = FormatGrid_Eingangspost(myGridEX, myGridName)
            End If

            If myGridName = "GridEXBarcodeProfil" Or _
                myGridName = "GridEXFreiterBarcodeTyp" Or _
                myGridName = "GridEXZugeordneteBarcodeTypen" Or _
                myGridName = "GridEXBarcodeGueltig" Or _
                myGridName = "GridEXBarcodeProtokoll" Then
                myGridEX = FormatGrid_ELOBarcode(myGridEX, myGridName)
            End If

            If myGridName = "GridEXLizenzen" Then
                myGridEX = FormatGrid_Lizenz(myGridEX, myGridName)
            End If


            Return myGridEX

        Catch ex As Exception

            modProtokoll.WriteProtokoll("modGridEX", "FormatGrid", "", ex.Message, 1, 0, 1, 0)
            MsgBox(ex.Message)

            Return myGridEX

        End Try


    End Function


    Private Function FormatGrid_Notification(myGridEX As GridEX, myGridName As String) As Janus.Windows.GridEX.GridEX

        Dim iPos As Integer

        iPos = 0

        If myGridName = "GridEXNotification" Then

            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("n_Bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True
                ' .ShowRowSelector = True
                '  .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

        End If



        If myGridName = "GridEXNotificationClient" Then

            myGridEX.GroupByBoxVisible = False

 
            With myGridEX.RootTable.Columns("nc_Name")
                .Width = 200
                .AllowSize = True
                .Caption = "Name"
                .Position = iPos
                .Visible = True
                '.ShowRowSelector = True
                ' .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("nc_Email")
                .Width = 200
                .AllowSize = True
                .Caption = "Email"
                .Position = iPos
                .Visible = True
                '.ShowRowSelector = True
                ' .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("nc_IstECDLink")
                .Width = 50
                .AllowSize = True
                .Caption = "ECD Link"
                .Position = iPos
                .Visible = True
                .CheckBoxTriState = False
                .EditType = EditType.CheckBox
                .ColumnType = ColumnType.CheckBox
                .CheckBoxFalseValue = CType(0, Int16)
                .CheckBoxTrueValue = CType(1, Int16)


            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("nc_IstAktiv")
                .Width = 50
                .AllowSize = True
                .Caption = "Aktiv"
                .Position = iPos
                .Visible = True
                .CheckBoxTriState = False
                .EditType = EditType.CheckBox
                .ColumnType = ColumnType.CheckBox
                .CheckBoxFalseValue = CType(0, Int16)
                .CheckBoxTrueValue = CType(1, Int16)


            End With


        End If

        FormatGrid_Notification = myGridEX

    End Function

    Private Function FormatGrid_Eingangspost(myGridEX As GridEX, myGridName As String) As Janus.Windows.GridEX.GridEX

        Dim iPos As Integer

        iPos = 0


        If myGridName = "GridEXBarcodes" Then
            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("barcode_id")
                .Visible = False
                .Position = iPos
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("bc_nummer")
                .Width = 80
                .AllowSize = False
                .Caption = "Barcode"
                .Position = iPos
                .Visible = True

            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("bc_beleg_datum")
                .Width = 90
                .AllowSize = False
                .Caption = "Datum"
                .Position = iPos
                .Visible = True
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("eingangspost_partner_id")
                .Width = 100
                .AllowSize = True
                .Caption = "Personenkonto"
                .Position = iPos
                .Visible = True
            End With

            iPos = iPos + 1


            With myGridEX.RootTable.Columns("bc_beleg_nummer")
                .Width = 100
                .AllowSize = False
                .Caption = "Beleg"
                .Position = iPos
                .Visible = True
            End With

            iPos = iPos + 1


            With myGridEX.RootTable.Columns("bc_vorgang_nummer")
                .Width = 100
                .AllowSize = False
                .Caption = "Vorgang"
                .Position = iPos
                .Visible = True
            End With



        End If

        If myGridName = "GridEXEingangspostTyp" Then

            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("eingangspost_typ_id")
                .Visible = False
                .Position = iPos
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("ept_bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True

            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("ept_partner_bezeichnung")
                .Visible = False
                .Position = iPos
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("ept_partner_bezeichnung_gruppe")
                .Visible = False
                .Position = iPos
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("ept_partner_bezeichnung_nummer")
                .Visible = False
                .Position = iPos
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("ept_partner_bezeichnung_nummer_eigen")
                .Visible = False
                .Position = iPos
            End With
        End If


        If myGridName = "GridEXEingangspostPartner" Then

            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("eingangspost_partner_id")
                .Visible = False
                .Position = iPos
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("epp_name")
                .Width = 100
                .AllowSize = True
                .Caption = "Name"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1


            With myGridEX.RootTable.Columns("epp_ort")
                .Width = 200
                .AllowSize = False
                .Caption = "Ort"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("epp_nummer")
                .Width = 100
                .AllowSize = False
                .Caption = "Nummer"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

            With myGridEX.RootTable.Columns("epp_nummer_eigen")
                .Width = 100
                .AllowSize = False
                .Caption = "Ihre Nummer"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With


        End If

        FormatGrid_Eingangspost = myGridEX

    End Function

    Private Function FormatGrid_Protokoll(myGridEX As GridEX, myGridName As String) As Janus.Windows.GridEX.GridEX

        Dim iPos As Integer

        iPos = 0

        myGridEX.GroupByBoxVisible = True

        With myGridEX.RootTable.Columns("ProtokollPK")
            .Visible = False
            .Position = iPos
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlProgramm")
            .Width = 150
            .AllowSize = False
            .Caption = "Programm"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlDatum")
            .Width = 150
            .AllowSize = False
            .Caption = "Datum"
            .Position = iPos
            .Visible = True
            .FormatString = "dd.MM.yyyy, HH:mm:ss"
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlSkript")
            .Width = 150
            .AllowSize = False
            .Caption = "Skript"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlAktion")
            .Width = 150
            .AllowSize = True
            .Caption = "Aktion"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlTyp")
            .Width = 100
            .AllowSize = True
            .Caption = "Typ"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlText")
            .Width = 100
            .AllowSize = True
            .Caption = "Text"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlIstFehler")
            .Width = 50
            .AllowSize = False
            .Caption = "Fehler"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlStatus")
            .Width = 50
            .AllowSize = False
            .Caption = "Status"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        iPos = iPos + 1

        With myGridEX.RootTable.Columns("PrtlCounter")
            .Width = 50
            .AllowSize = False
            .Caption = "Counter"
            .Position = iPos
            .Visible = True
            .EditType = EditType.NoEdit
        End With

        FormatGrid_Protokoll = myGridEX

    End Function

    Private Function FormatGrid_Lizenz(myGridEX As GridEX, myGridName As String) As Janus.Windows.GridEX.GridEX

        Dim iPos As Integer

        iPos = 0

        If myGridName = "GridEXLizenzen" Then

            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("l_Bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True
                .ShowRowSelector = True
                .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1


            With myGridEX.RootTable.Columns("l_Code")
                .Width = 100
                .AllowSize = False
                .Caption = "Code"
                .Position = iPos
                .Visible = True
                '.EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

        End If

        FormatGrid_Lizenz = myGridEX

    End Function

    Private Function FormatGrid_ELOBarcode(myGridEX As GridEX, myGridName As String) As Janus.Windows.GridEX.GridEX

        Dim iPos As Integer

        iPos = 0

        If myGridName = "GridEXBarcodeProfil" Then

            myGridEX.GroupByBoxVisible = False
            myGridEX.AllowAddNew = InheritableBoolean.False
            myGridEX.AllowEdit = InheritableBoolean.False


            With myGridEX.RootTable.Columns("bp_bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True
                '.EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

        End If

        If myGridName = "GridEXFreiterBarcodeTyp" Then

            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("bct_bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

        End If

        If myGridName = "GridEXZugeordneteBarcodeTypen" Then

            myGridEX.GroupByBoxVisible = False

            With myGridEX.RootTable.Columns("bct_bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With

            iPos = iPos + 1

        End If



        If myGridName = "GridEXBarcodeGueltig" Then

            myGridEX.GroupByBoxVisible = False
            myGridEX.AllowAddNew = InheritableBoolean.True
            myGridEX.AllowDelete = InheritableBoolean.True
            myGridEX.AllowEdit = InheritableBoolean.True
            myGridEX.RowHeaders = InheritableBoolean.True


            With myGridEX.RootTable.Columns("gbc_bezeichnung")
                .Width = 100
                .AllowSize = True
                .Caption = "Bezeichnung"
                .Position = iPos
                .Visible = True
                .EditType = EditType.TextBox
            End With

            iPos = iPos + 1


            With myGridEX.RootTable.Columns("gbc_praefix")
                .Width = 50
                .AllowSize = False
                .Caption = "Präfix"
                .Position = iPos
                .Visible = True
                .EditType = EditType.TextBox
            End With

            iPos = iPos + 1


            With myGridEX.RootTable.Columns("gbc_suffix")
                .Width = 50
                .AllowSize = False
                .Caption = "Suffix"
                .Position = iPos
                .Visible = True
                .EditType = EditType.TextBox
            End With

            iPos = iPos + 1

        End If

        If myGridName = "GridEXBarcodeProtokoll" Then
            myGridEX.GroupByBoxVisible = False
            myGridEX.AllowAddNew = InheritableBoolean.False
            myGridEX.AllowDelete = InheritableBoolean.False
            myGridEX.AllowEdit = InheritableBoolean.False
            myGridEX.ColumnAutoResize = True


            With myGridEX.RootTable.Columns("PrtlSkript")
                .Width = 100
                .AllowSize = False
                .Caption = "Skript"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With
            iPos += 1
            With myGridEX.RootTable.Columns("PrtlDatum")
                .Width = 150
                .AllowSize = False
                .Caption = "Zeitpunkt"
                .Position = iPos
                .Visible = True
                .FormatString = "dd.MM.yyyy, HH:mm:ss"
                .EditType = EditType.NoEdit
            End With
            iPos += 1
            With myGridEX.RootTable.Columns("PrtlAktion")
                .Width = 100
                .AllowSize = False
                .Caption = "Aktion"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With
            iPos += 1
            With myGridEX.RootTable.Columns("PrtlText")
                .Width = 600
                .AllowSize = True
                .Caption = "Text"
                .Position = iPos
                .Visible = True
                .EditType = EditType.NoEdit
            End With
            iPos += 1
        End If

        Return myGridEX

    End Function


End Module
