﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Reflection

Public Module modAllgemein

    Public iProtokollLevel As Integer

    Public Function SetProcessStatus(strProcess As String, strStatus As String) As String

        Try

            SetProcessStatus = ""


            Dim SQLstr As String
            Dim cn As New SqlConnection(Replace(My.Settings.ConnectionString, "Provider=SQLOLEDB;", ""))

            SQLstr = "EXECUTE prc_SetProcessStatus " & _
                     "@Process=@process, @Status=@status"

            cn.Open()
            Dim sqlCMD As SqlCommand = New SqlCommand(SQLstr, cn)

            sqlCMD.Parameters.Add("@process", SqlDbType.VarChar).Value = strProcess
            sqlCMD.Parameters.Add("@status", SqlDbType.VarChar).Value = strStatus

            sqlCMD.ExecuteNonQuery()

            cn.Close()
            cn.Dispose()

        Catch ex As Exception


            SetProcessStatus = "ERR"
            modProtokoll.WriteProtokoll("modAllgemein", "SetProzess", "", ex.Message, 1, 0, 1, 0)
            MsgBox(ex.Message)
        End Try

    End Function


    Public Function GetProcessStatus(strProcess As String) As String

        Try

            GetProcessStatus = ""


            Dim SQLstr As String
            Dim cn As New SqlConnection(Replace(My.Settings.ConnectionString, "Provider=SQLOLEDB;", ""))

            SQLstr = "EXECUTE prc_GetProcessStatus " & _
                     "@Process=@process"

            cn.Open()
            Dim sqlCMD As SqlCommand = New SqlCommand(SQLstr, cn)

            sqlCMD.Parameters.Add("@process", SqlDbType.VarChar).Value = strProcess

            GetProcessStatus = sqlCMD.ExecuteScalar()

            cn.Close()

        Catch ex As Exception


            GetProcessStatus = ""
            modProtokoll.WriteProtokoll("modAllgemein", "SetProzess", "", ex.Message, 1, 0, 1, 0)
            MsgBox(ex.Message)
        End Try

    End Function


    Public Function GetTimeStamp() As String

        GetTimeStamp = Replace(Replace(Replace(Replace(Replace(FormatDateTime(Now(), DateFormat.GeneralDate), ":", ""), ".", ""), " ", ""), "/", ""), "\", "")

    End Function


    Public Function GetDbValueString(strFieldName As String, strTableName As String, strKeyField As String, strKeyValue As String) As String

        Try

            'modProtokoll.WriteProtokoll("modAllgemein", "GetDbValueString", "Start", "strFieldName=" & strFieldName & ", strTableName=" & strTableName & ", strKeyField=" & strKeyField & ", strKeyValue=" & strKeyValue, 0, 0, 1, 1)

            GetDbValueString = ""

            Dim SQLstr As String
            Dim cn As New SqlConnection(Replace(My.Settings.ConnectionString, "Provider=SQLOLEDB;", ""))

            SQLstr = "SELECT " & strFieldName & " FROM " & strTableName & " WHERE " & strKeyField & "='" & strKeyValue & "' "
            cn.Open()
            Dim sqlCMD As SqlCommand = New SqlCommand(SQLstr, cn)
            GetDbValueString = sqlCMD.ExecuteScalar()
            cn.Close()

            'modProtokoll.WriteProtokoll("modAllgemein", "GetDbValueString", "Ende", "strFieldName=" & strFieldName & ", strTableName=" & strTableName & ", strKeyField=" & strKeyField & ", strKeyValue=" & strKeyValue, 0, 0, 1, 1)

        Catch ex As Exception

            GetDbValueString = ""
            modProtokoll.WriteProtokoll("modAllgemein", "GetDbValueString", "", ex.Message, 1, 0, 1, 0)
            'MsgBox(ex.Message)
        End Try

    End Function


    Public Sub DateienVerschieben(sQuelle As String, sZiel As String)

        On Error GoTo Errhandler

        Dim dest As String
        Dim files As String()
        Dim fileName As String
        Dim destFile As String


        dest = Microsoft.VisualBasic.Left(sZiel, Len(sZiel) - 1)
        files = System.IO.Directory.GetFiles(Microsoft.VisualBasic.Left(sQuelle, Len(sQuelle) - 1))

        For Each s In files
            fileName = System.IO.Path.GetFileName(s)
            destFile = System.IO.Path.Combine(dest, fileName)

            While File.Exists(destFile) = True
                destFile = Replace(destFile, ".xml", "_" & modAllgemein.GetTimeStamp() & ".xml")
            End While

            modProtokoll.WriteProtokoll("modAllgemein", "DateienVerschieben", "OK", "Quelle: " & sQuelle & " Ziel: " & sZiel, 0, 5, 1, 2)

            File.Move(s, destFile)
        Next

        Exit Sub


Errhandler:

        modProtokoll.WriteProtokoll("modAllgemein", "DateienVerschieben", "ERR", "Quelle: " & sQuelle & " Ziel: " & sZiel, 1, 0, 1, 0)
        Resume Next


    End Sub

    Public Function ValidateFolder(sPfad As String, bCreateBackupFolder As Boolean, Optional bCreateBackupFolderForBarcode As Boolean = False) As String

        Try


            If Microsoft.VisualBasic.Right(sPfad, 1) <> "\" Then
                sPfad = sPfad & "\"
            End If

            If System.IO.Directory.Exists(sPfad) = False Then
                System.IO.Directory.CreateDirectory(sPfad)
            End If

            If bCreateBackupFolder = True Then
                If System.IO.Directory.Exists(sPfad & "Backup\") = False Then
                    System.IO.Directory.CreateDirectory(sPfad & "Backup\")
                End If
            End If

            ValidateFolder = sPfad

        Catch ex As Exception

            ValidateFolder = sPfad
            modProtokoll.WriteProtokoll("modAllgemein", "ValidateFolder", "Fehler", "sPfad=" & sPfad & ", bCreateBackupFolder=" & bCreateBackupFolder & ", Message=" & ex.Message, 1, 0, 1, 0)
            MsgBox(ex.Message)
        End Try

    End Function

End Module

Public Class ProgrammInfo
    Private assembly As Assembly = assembly.GetExecutingAssembly()
    Private fvi As FileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location)

    ReadOnly Property VersionNumber() As String
        Get
            Return fvi.FileVersion
        End Get
    End Property
    ReadOnly Property ProductName() As String
        Get
            Return fvi.ProductName
        End Get
    End Property
    ReadOnly Property GetCompileDate() As DateTime
        Get
            Return RetrieveLinkerTimestamp(assembly.GetExecutingAssembly.Location)
        End Get
    End Property

    Private Function RetrieveLinkerTimestamp(ByVal filePath As String) As DateTime

        Const PortableExecutableHeaderOffset As Integer = 60
        Const LinkerTimestampOffset As Integer = 8

        Dim b(2047) As Byte
        Dim s As IO.Stream = Nothing

        Try
            s = New IO.FileStream(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            s.Read(b, 0, 2048)
        Finally
            If Not s Is Nothing Then s.Close()
        End Try

        Dim i As Integer = BitConverter.ToInt32(b, PortableExecutableHeaderOffset)
        Dim secondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
        Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)

        dt = dt.AddSeconds(secondsSince1970)
        dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

        Return dt

    End Function
End Class