﻿Imports Janus.Windows.GridEX
Imports System.Data.SqlClient
Imports System.Management
Imports System.Security.Cryptography



Public NotInheritable Class Simple3Des


    Private TripleDes As New TripleDESCryptoServiceProvider


    Private Function TruncateHash(ByVal key As String, ByVal length As Integer) As Byte()

        Dim sha1 As New SHA1CryptoServiceProvider

        ' Hash the key.
        Dim keyBytes() As Byte = _
            System.Text.Encoding.Unicode.GetBytes(key)
        Dim hash() As Byte = sha1.ComputeHash(keyBytes)

        ' Truncate or pad the hash.
        ReDim Preserve hash(length - 1)
        Return hash
    End Function

    Sub New(ByVal key As String)
        ' Initialize the crypto provider.
        TripleDes.Key = TruncateHash(key, TripleDes.KeySize \ 8)
        TripleDes.IV = TruncateHash("", TripleDes.BlockSize \ 8)
    End Sub

    Public Function EncryptData(ByVal plaintext As String) As String

        ' Convert the plaintext string to a byte array.
        Dim plaintextBytes() As Byte = _
            System.Text.Encoding.Unicode.GetBytes(plaintext)

        ' Create the stream.
        Dim ms As New System.IO.MemoryStream
        ' Create the encoder to write to the stream.
        Dim encStream As New CryptoStream(ms, _
            TripleDes.CreateEncryptor(), _
            System.Security.Cryptography.CryptoStreamMode.Write)

        ' Use the crypto stream to write the byte array to the stream.
        encStream.Write(plaintextBytes, 0, plaintextBytes.Length)
        encStream.FlushFinalBlock()

        ' Convert the encrypted stream to a printable string.
        Return Convert.ToBase64String(ms.ToArray)
    End Function


    Public Function DecryptData(ByVal encryptedtext As String) As String

        ' Convert the encrypted text string to a byte array.
        Dim encryptedBytes() As Byte = Convert.FromBase64String(encryptedtext)

        ' Create the stream.
        Dim ms As New System.IO.MemoryStream
        ' Create the decoder to write to the stream.
        Dim decStream As New CryptoStream(ms, _
            TripleDes.CreateDecryptor(), _
            System.Security.Cryptography.CryptoStreamMode.Write)

        ' Use the crypto stream to write the byte array to the stream.
        decStream.Write(encryptedBytes, 0, encryptedBytes.Length)
        decStream.FlushFinalBlock()

        ' Convert the plaintext stream to a string.
        Return System.Text.Encoding.Unicode.GetString(ms.ToArray)
    End Function


 



    Public Function GetSerialNumber() As String

        Dim serialGuid As Guid = Guid.NewGuid()
        Dim uniqueSerial As String = serialGuid.ToString("N")
        Dim uniqueSerialLength As String = uniqueSerial.Substring(0, 28).ToUpper()
        Dim serialArray As Char() = uniqueSerialLength.ToCharArray()
        Dim finalSerialNumber As String = ""
        Dim j As Integer = 0
        For i As Integer = 0 To 27
            For j = i To 4 + (i - 1)
                finalSerialNumber += serialArray(j)
            Next
            If j = 28 Then
                Exit For
            Else
                i = (j) - 1
                finalSerialNumber += "-"
            End If
        Next

        Return finalSerialNumber

    End Function

End Class


Public Module modLizenz


    Public Sub EncodingLicence(strLizenz As String, strKennwort As String)

        Try

            Dim wrapper As New Simple3Des(strKennwort)
            Dim cipherText As String = wrapper.EncryptData(strLizenz)

            MsgBox("The cipher text is: " & cipherText)
            My.Computer.FileSystem.WriteAllText("C:\SiConnectLizenz\SiConnect.lic", cipherText, False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Public Function DecodingLicence(strKennwort As String, strPfad As String) As String

        If Not strPfad.EndsWith("\") Then strPfad = strPfad & "\"

        Dim cipherText As String = My.Computer.FileSystem.ReadAllText(strPfad & "SiConnect.lic")
        Dim wrapper As New Simple3Des(strKennwort)

        Try
            Dim plainText As String = wrapper.DecryptData(cipherText)

            DecodingLicence = plainText
        Catch ex As System.Security.Cryptography.CryptographicException
            DecodingLicence = ""
            MsgBox("The data could not be decrypted with the password.")
        End Try
    End Function


    Public Function GetSystemKey() As String

        Dim searcher As New ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive")

        Try
            For Each wmi_HD As ManagementObject In searcher.Get()
                If wmi_HD.Item("Index") = 0 Then
                    ' wenn Index gefunden, Signature-Property zurückgeben
                    Return Microsoft.VisualBasic.Left(wmi_HD.Item("Signature").ToString, 5)
                End If
            Next wmi_HD
        Catch
            ' Im Fehlerfall Leerstring
            Return "NoKey"
        End Try

        ' Falls Index nicht gefunden, Leerstring zurückgeben
        Return "NoKey"
    End Function

End Module