﻿Imports System.Data.SqlClient

Public Module modProtokoll


    Public Function WriteProtokoll(PrtlSkript As String, PrtlAktion As String, PrtlTyp As String, PrtlText As String, PrtlIstFehler As Integer, PrtlStatus As Integer, PrtlCounter As Integer, iPrtlLevel As Integer) As String

        Try


            Dim conStr As String

            If iPrtlLevel <= modAllgemein.iProtokollLevel Then
                'conStr = Replace(My.Settings.ConnectionString, "Provider=SQLOLEDB;", "")

                'Dim sqlConnection1 As New SqlConnection(conStr)
                'sqlConnection1.Open()

                'Dim sqlCMD As SqlCommand = New SqlCommand("EXECUTE prc_ELOProtokoll " &
                '                                         "@PrtlArchiv	    = '', " &
                '                                         "@PrtlWorkstation	= '', " &
                '                                         "@PrtlBenutzer     = '', " &
                '                                         "@PrtlProgramm 	= @PrtlProgramm, " &
                '                                         "@PrtlSkript	    = @PrtlSkript, " &
                '                                         "@PrtlAktion	    = @PrtlAktion, " &
                '                                         "@PrtlTyp	        = @PrtlTyp, " &
                '                                         "@PrtlText		    = @PrtlText, " &
                '                                         "@PrtlIstFehler	= @PrtlIstFehler, " &
                '                                         "@PrtlStatus       = @PrtlStatus, " &
                '                                         "@PrtlCounter	    = @PrtlCounter", sqlConnection1)


                'sqlCMD.Parameters.Add("@PrtlProgramm", SqlDbType.VarChar, 50).Value = "SiEloConnect"
                'sqlCMD.Parameters.Add("@PrtlWorkstation", SqlDbType.VarChar, 50).Value = ""
                'sqlCMD.Parameters.Add("@PrtBenutzer", SqlDbType.VarChar, 50).Value = ""
                'sqlCMD.Parameters.Add("@PrtlSkript", SqlDbType.VarChar, 150).Value = PrtlSkript
                'sqlCMD.Parameters.Add("@PrtlAktion", SqlDbType.VarChar, 150).Value = PrtlAktion
                'sqlCMD.Parameters.Add("@PrtlTyp", SqlDbType.VarChar, 50).Value = PrtlTyp
                'sqlCMD.Parameters.Add("@PrtlText", SqlDbType.VarChar).Value = PrtlText
                'sqlCMD.Parameters.Add("@PrtlIstFehler", SqlDbType.Int).Value = PrtlIstFehler
                'sqlCMD.Parameters.Add("@PrtlStatus ", SqlDbType.Int).Value = PrtlStatus
                'sqlCMD.Parameters.Add("@PrtlCounter ", SqlDbType.Int).Value = PrtlCounter

                ''sqlCMD.ExecuteScalar()

                'sqlConnection1.Close()
                'sqlConnection1.Dispose()
            End If
            Return "OK"
        Catch ex As Exception
            MsgBox(ex.Message)
            Return "NOK"
        End Try


    End Function


End Module
