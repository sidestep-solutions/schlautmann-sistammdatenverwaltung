﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInfo
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInfo))
        Me.btnClose = New Janus.Windows.EditControls.UIButton()
        Me.UiTab1 = New Janus.Windows.UI.Tab.UITab()
        Me.UiTabPage1 = New Janus.Windows.UI.Tab.UITabPage()
        Me.txtVersion = New Janus.Windows.GridEX.EditControls.EditBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSystemschluessel = New Janus.Windows.GridEX.EditControls.EditBox()
        Me.txtSideStepInfo = New Janus.Windows.GridEX.EditControls.EditBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.UiTab1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UiTab1.SuspendLayout()
        Me.UiTabPage1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(310, 342)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Schliessen"
        '
        'UiTab1
        '
        Me.UiTab1.Location = New System.Drawing.Point(16, 137)
        Me.UiTab1.Name = "UiTab1"
        Me.UiTab1.Size = New System.Drawing.Size(369, 199)
        Me.UiTab1.TabIndex = 1
        Me.UiTab1.TabPages.AddRange(New Janus.Windows.UI.Tab.UITabPage() {Me.UiTabPage1})
        Me.UiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Normal
        '
        'UiTabPage1
        '
        Me.UiTabPage1.Controls.Add(Me.txtVersion)
        Me.UiTabPage1.Controls.Add(Me.Label2)
        Me.UiTabPage1.Controls.Add(Me.Label1)
        Me.UiTabPage1.Controls.Add(Me.txtSystemschluessel)
        Me.UiTabPage1.Controls.Add(Me.txtSideStepInfo)
        Me.UiTabPage1.Location = New System.Drawing.Point(2, 22)
        Me.UiTabPage1.Name = "UiTabPage1"
        Me.UiTabPage1.Size = New System.Drawing.Size(365, 175)
        Me.UiTabPage1.TabStop = True
        Me.UiTabPage1.Text = "Copyright und Lizenzinformation"
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = Janus.Windows.GridEX.BorderStyle.None
        Me.txtVersion.Location = New System.Drawing.Point(96, 54)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.Size = New System.Drawing.Size(264, 18)
        Me.txtVersion.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Version:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 81)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Systemschlüssel:"
        '
        'txtSystemschluessel
        '
        Me.txtSystemschluessel.BackColor = System.Drawing.SystemColors.Control
        Me.txtSystemschluessel.BorderStyle = Janus.Windows.GridEX.BorderStyle.None
        Me.txtSystemschluessel.Location = New System.Drawing.Point(96, 79)
        Me.txtSystemschluessel.Name = "txtSystemschluessel"
        Me.txtSystemschluessel.ReadOnly = True
        Me.txtSystemschluessel.Size = New System.Drawing.Size(264, 18)
        Me.txtSystemschluessel.TabIndex = 2
        '
        'txtSideStepInfo
        '
        Me.txtSideStepInfo.BackColor = System.Drawing.SystemColors.Control
        Me.txtSideStepInfo.BorderStyle = Janus.Windows.GridEX.BorderStyle.None
        Me.txtSideStepInfo.ControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Flat
        Me.txtSideStepInfo.ControlThemedAreas = Janus.Windows.GridEX.ControlThemedAreas.None
        Me.txtSideStepInfo.FlatBorderColor = System.Drawing.Color.Transparent
        Me.txtSideStepInfo.Location = New System.Drawing.Point(6, 103)
        Me.txtSideStepInfo.Multiline = True
        Me.txtSideStepInfo.Name = "txtSideStepInfo"
        Me.txtSideStepInfo.ReadOnly = True
        Me.txtSideStepInfo.Size = New System.Drawing.Size(356, 60)
        Me.txtSideStepInfo.TabIndex = 1
        Me.txtSideStepInfo.Text = "ttt"
        Me.txtSideStepInfo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox2.Image = Global.SiStammdatenverwaltung.My.Resources.Resources.SideStep3_cmyk300__3_
        Me.PictureBox2.Location = New System.Drawing.Point(16, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(369, 119)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 4
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.WaitOnLoad = True
        '
        'frmInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(403, 374)
        Me.Controls.Add(Me.UiTab1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.btnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Info"
        CType(Me.UiTab1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UiTab1.ResumeLayout(False)
        Me.UiTabPage1.ResumeLayout(False)
        Me.UiTabPage1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As Janus.Windows.EditControls.UIButton
    Friend WithEvents UiTab1 As Janus.Windows.UI.Tab.UITab
    Friend WithEvents UiTabPage1 As Janus.Windows.UI.Tab.UITabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSystemschluessel As Janus.Windows.GridEX.EditControls.EditBox
    Friend WithEvents txtSideStepInfo As Janus.Windows.GridEX.EditControls.EditBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents txtVersion As Janus.Windows.GridEX.EditControls.EditBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
