﻿Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports Janus.Windows.EditControls
Imports Janus.Windows.Ribbon

Public Class frmMain
    Dim connection As New SqlConnection
    Dim strCon As String = My.Settings.ConnectionString
    Dim reader As SqlDataReader

    'Konstanten müssen für jede Tabelle angepasst werden.

    'Konstanten für Tabelle Kunde und KundeExt:
    Const tablenameERPKunde = "'t_ERPKunde'"
    Const tablenameExtERPKunde = "'t_ERPKundeExt'"
    Const fulltablenameERPKunde = "dbo.t_ERPKunde"
    Const fulltablenameExtERPKunde = "dbo.t_ERPKundeExt"
    Const nameOverviewERPKunde = ", Name1"
    Const profilERPKunde = "ERPKunde"
    Const ERPKundeExtExists = True

    'Konstanten für Tabelle Lieferant und LieferantExt:
    Const tablenameERPLieferant = "'t_ERPlieferant'"
    Const tablenameExtERPLieferant = "'t_ERPlieferantExt'"
    Const fulltablenameERPLieferant = "dbo.t_ERPlieferant"
    Const fulltablenameExtERPLieferant = "dbo.t_ERPlieferantExt"
    Const nameOverviewERPLieferant = ", Name1"
    Const profilERPLieferant = "ERPLieferant"
    Const ERPLieferantExtExists = True

    'Konstanten für Tabelle Personal und PersonalExt:
    Const tablenameERPPersonal = "'t_ERPPersonal'"
    Const tablenameExtERPPersonal = "'t_ERPPersonalExt'"
    Const fulltablenameERPPersonal = "dbo.t_ERPPersonal"
    Const fulltablenameExtERPPersonal = "dbo.t_ERPPersonalExt"
    Const nameOverviewERPPersonal = ", Name1"
    Const profilERPPersonal = "ERPPersonal"
    Const ERPPersonalExtExists = True

    'Konstanten für Tabelle Test (für Experimente):
    Const tablenameERPTest = "'t_ERPTest'"
    Const fulltablenameERPTest = "dbo.t_ERPTest"
    Const nameOverviewERPTest = ", datetime"
    Const profilERPTest = "ERPTest"
    Const ERPTestExtExists = False

    'Hier alle Tabellennamen für die Auswahl eintragen
    'Combobox im AllgemeinTab deaktiviert wegen dynmaischer Taberstellung
    'Dim TablesComboBox = {New UIComboBoxItem(profilERPKunde), New UIComboBoxItem(profilERPLieferant), New UIComboBoxItem(profilERPPersonal)}


    'Tabnamen für dynamische taberstellung
    'Dim TablesTabs = {profilERPKunde, profilERPLieferant, profilERPPersonal, profilERPTest}
    'Dim TablesTabs = {profilERPKunde, profilERPLieferant, profilERPPersonal}
    Dim TablesTabs = {profilERPKunde, profilERPLieferant}


    'startTabelle ERPKunde
    Dim tablename = tablenameERPKunde
    Dim tablenameExt = tablenameExtERPKunde
    Dim fulltablename = fulltablenameERPKunde
    Dim fulltablenameExt = fulltablenameExtERPKunde
    Dim nameOverview = nameOverviewERPKunde
    Dim profil = profilERPKunde
    Dim ExtExists = ERPKundeExtExists


    Dim newMandant = "Immobilien"


    Dim unsavedChanges = False

    Dim mandant = "''"
    Dim nummer = "''"
    Dim number As Integer
    Dim newnumber = False

    'Felder die nicht geändert werden können oder automatisch geändert werden
    'Dim untouchables = {"Mandant", "Nummer", "Profil", "Erstellt", "Geaendert"}

    Dim strCountColumns As String
    Dim strCountColumnsExt As String '
    Dim columnCommand As SqlCommand
    Dim columns As New DataTable
    Dim dynamicColumnNames As String = ""
    Dim dynamicColumnNamesExt As String = ""
    Dim pi As New ProgrammInfo()

    Dim columnnames As ArrayList
    Dim columnnamesExt As ArrayList

    Dim strCommand As String
    Dim strCommandExt As String

    'FORM LOAD
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = pi.ProductName + " ∙ " + pi.VersionNumber + " ∙ " + pi.GetCompileDate.Date
        CalculateDynamicColumnNames()
        CalculateDynamicColumnNamesExt()

        Try
            Dim strCommandOverview = "SELECT Mandant, Nummer " + nameOverview + " FROM " + fulltablename
            Dim overview As New DataTable
            overview = RetrieveDataTable(strCommandOverview, "Overview")

            Me.GridEXOverview.SetDataBinding(overview, "")
            Me.GridEXOverview.RetrieveStructure()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error loadOverview")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try

        For Each tableprofile In TablesTabs
            CreateTab(tableprofile)
        Next
    End Sub

    'DYNAMIC
    Private Sub SelectTable(selectedItem As String)
        Select selectedItem
            Case profilERPKunde
                tablename = tablenameERPKunde
                tablenameExt = tablenameExtERPKunde
                fulltablename = fulltablenameERPKunde
                fulltablenameExt = fulltablenameExtERPKunde
                nameOverview = nameOverviewERPKunde
                profil = profilERPKunde
                ExtExists = ERPKundeExtExists


            Case profilERPLieferant
                tablename = tablenameERPLieferant
                tablenameExt = tablenameExtERPLieferant
                fulltablename = fulltablenameERPLieferant
                fulltablenameExt = fulltablenameExtERPLieferant
                nameOverview = nameOverviewERPLieferant
                profil = profilERPLieferant
                ExtExists = ERPLieferantExtExists

            Case profilERPPersonal
                tablename = tablenameERPPersonal
                tablenameExt = tablenameExtERPPersonal
                fulltablename = fulltablenameERPPersonal
                fulltablenameExt = fulltablenameExtERPPersonal
                nameOverview = nameOverviewERPPersonal
                profil = profilERPPersonal
                ExtExists = ERPPersonalExtExists

            Case profilERPTest
                tablename = tablenameERPTest
                fulltablename = fulltablenameERPTest
                nameOverview = nameOverviewERPTest
                profil = profilERPTest
                ExtExists = ERPPersonalExtExists
        End Select


        'IN ARBEIT--------------------------------------------------------------------------------------

        'profil = selectedItem
        'tablename = GetTablename(selectedItem)
        'tablenameExt = GetTablenameExt(selectedItem)
        'fulltablename = GetFullTablename(selectedItem)
        'fulltablenameExt = GetFullTablenameExt(selectedItem)
        'nameOverview = ", name1"

        'ExtExists = True

        'Try
        '    Dim Existance = False
        '    Dim checkcommand As String = ""
        '    checkcommand = "select * from master.dbo.sysdatabases where name = "
        '    checkcommand += "'" + fulltablename + "'"
        '    connection.Open()
        '    Dim sqlcmd As SqlCommand
        '    sqlcmd = New SqlCommand(checkcommand, connection)
        '    sqlcmd.CommandTimeout = 3000
        '    Using reader As SqlDataReader = sqlcmd.ExecuteReader
        '        Existance = reader.HasRows
        '    End Using
        '    ExtExists = Existance
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Error CheckExtExistance")
        'Finally
        '    If connection.State = ConnectionState.Open Then
        '        connection.Close()
        '    End If
        'End Try
        '-----------------------------------------------------------------------------------------------



        CalculateDynamicColumnNames()
        CalculateDynamicColumnNamesExt()
        RefreshOverview()
    End Sub

    Private Sub CalculateDynamicColumnNames()
        Try
            strCountColumns = "select c.name FROM sys.tables t LEFT JOIN sys.columns c ON t.object_id = c.object_id where t.name = " + tablename
            connection.ConnectionString = strCon
            columnCommand = New SqlCommand(strCountColumns, connection)
            columnCommand.CommandTimeout = 3000
            connection.Open()
            reader = columnCommand.ExecuteReader()
            columnnames = New ArrayList
            Dim firstentry = True
            While reader.Read()
                If firstentry Then
                    firstentry = False
                    dynamicColumnNames = "" + reader(0)
                Else
                    dynamicColumnNames += (", " + reader(0))
                End If
                columnnames.Add(reader(0))
            End While
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error dynamic")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub CalculateDynamicColumnNamesExt()
        If ExtExists Then         
            Try
                strCountColumnsExt = "select c.name FROM sys.tables t LEFT JOIN sys.columns c ON t.object_id = c.object_id where t.name = " + tablenameExt
                columnCommand = New SqlCommand(strCountColumnsExt, connection)
                columnCommand.CommandTimeout = 3000
                connection.Open()
                reader = columnCommand.ExecuteReader()
                columnnamesExt = New ArrayList
                Dim firstentry = True
                While reader.Read()
                    If firstentry Then
                        firstentry = False
                        dynamicColumnNamesExt = "" + reader(0)
                    Else
                        dynamicColumnNamesExt += (", " + reader(0))
                    End If
                    columnnamesExt.Add(reader(0))
                End While
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error dynamicExt")
            Finally
                If connection.State = ConnectionState.Open Then
                    connection.Close()
                End If
            End Try
        End If

    End Sub


    'DELETE
    Private Sub DeleteRow_Click(sender As System.Object, e As Janus.Windows.Ribbon.CommandEventArgs) Handles btnDeleteRow.Click
        DeleteRow()
        DeleteRowExt()
        RefreshOverview()
    End Sub

    Private Sub DeleteRow()
        Try
            Dim kundendaten As New DataTable
            kundendaten = RetrieveDataTable(strCommand, tablename)
     
            Dim rowToLoad = kundendaten.NewRow()
            rowToLoad("Nummer") = GridEXOverview.CurrentRow.Cells("nummer").Value
            rowToLoad("Mandant") = GridEXOverview.CurrentRow.Cells("mandant").Value
            rowToLoad("Profil") = profil


            Dim row As DataRow = kundendaten.LoadDataRow(rowToLoad.ItemArray(), True)
            row.Delete()

            DeleteDataTable(kundendaten, strCommand)  

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ErrorDeleteRow")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub DeleteRowExt()
        If ExtExists Then
            Try
                Dim kundendatenExt As New DataTable

                kundendatenExt = RetrieveDataTable(strCommandExt, tablenameExt)

                Dim gMNPExt(3) As Object
                gMNPExt(0) = GridEXOverview.CurrentRow.Cells("nummer").Value
                gMNPExt(1) = GridEXOverview.CurrentRow.Cells("mandant").Value
                gMNPExt(2) = profil

                Dim rowExt As DataRow = kundendatenExt.LoadDataRow(gMNPExt, True)
                rowExt.Delete()

                DeleteDataTable(kundendatenExt, strCommandExt)


            Catch ex As Exception
                MessageBox.Show(ex.Message, "ErrorDeleteRowExt")
            Finally
                If connection.State = ConnectionState.Open Then
                    connection.Close()
                End If
            End Try
        End If


    End Sub


    'UPDATE
    Private Sub AlterRow_Click(sender As System.Object, e As Janus.Windows.Ribbon.CommandEventArgs) Handles btnAlterRow.Click
        'TODO end edit 
        'OverviewBindingSource.EndEdit()
        Validate()

        AlterRow()
        AlterRowExt()
        unsavedChanges = False
        btnAlterRow.Enabled = False
        RefreshOverview()
    End Sub

    Private Sub AlterRow()
        Try
            Dim kundendaten As New DataTable
            kundendaten = RetrieveDataTable(strCommand, tablename)

            Dim typeArray = AnalyseTable_datatype(tablename)

            Dim rowToLoad = kundendaten.NewRow()
            rowToLoad("Nummer") = GridEXOverview.CurrentRow.Cells("nummer").Value
            rowToLoad("Mandant") = GridEXOverview.CurrentRow.Cells("mandant").Value
            rowToLoad("Profil") = profil

            Dim row As DataRow = kundendaten.LoadDataRow(rowToLoad.ItemArray(), True)
            row.BeginEdit()

            Dim i = 0
            For Each element As Janus.Windows.GridEX.GridEXCell In GridEXMain.CurrentRow.Cells
                If columnnames(i) = "Mandant" Or columnnames(i) = "Nummer" Or columnnames(i) = "Profil" Or columnnames(i) = "Erstellt" Then
                ElseIf columnnames(i) = "Profil" Or columnnames(i) = "Erstellt" Then
                ElseIf typeArray(i) = "datetime" Then
                    If columnnames(i) = "Geaendert" Then
                        row.Item("Geaendert") = DateTime.Today
                    ElseIf element.Value.Equals(DBNull.Value) Then
                        row(i) = DBNull.Value
                    Else
                        row(i) = DateTime.Parse(element.Value)
                    End If
                    'ElseIf typeArray(i) = "int" Then
                    '   row(i) = Integer.TryParse(element.Value, 0)
                ElseIf element.Value.Equals(DBNull.Value) Then
                    row(i) = DBNull.Value
                Else
                    row(i) = element.Value
                End If
                i += 1
            Next

            row.EndEdit()

            UpdateDataTable(kundendaten, strCommand)
        Catch sqlex As SqlException
            MessageBox.Show(sqlex.Message + " Die Eingaben in einem Feld sind länger als in der Datenbank erlaubt: Fassen Sie sich kürzer oder lassen Sie die Datenbankspalten verbreitern", "SQL-Datenbank-Fehler in Tabelle: " + profil)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error AlterRow")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub AlterRowExt()
        If ExtExists Then
            Try
                Dim kundendatenExt As New DataTable
                kundendatenExt = RetrieveDataTable(strCommandExt, tablenameExt)

                Dim gNMPExt(2) As Object
                gNMPExt(0) = GridEXOverview.CurrentRow.Cells("nummer").Value
                gNMPExt(1) = GridEXOverview.CurrentRow.Cells("mandant").Value
                gNMPExt(2) = profil


                Dim rowExt As DataRow = kundendatenExt.LoadDataRow(gNMPExt, True)
                rowExt.BeginEdit()

                Dim i = 0

                For Each element As Janus.Windows.GridEX.GridEXCell In GridEXExt.CurrentRow.Cells
                    If columnnamesExt(i) = "Mandant" Or columnnamesExt(i) = "Nummer" Or columnnamesExt(i) = "Profil" Then
                    ElseIf columnnamesExt(i) = "Profil" Then
                    ElseIf columnnamesExt(i) = "Nummer" Then
                        rowExt(i) = GridEXMain.CurrentRow.Cells("nummer").Value
                    ElseIf columnnamesExt(i) = "Mandant" Then
                        rowExt(i) = GridEXMain.CurrentRow.Cells("mandant").Value
                    ElseIf element.Value.Equals(DBNull.Value) Then
                        rowExt(i) = ""
                    Else
                        rowExt(i) = element.Value
                    End If
                    i += 1
                Next

                rowExt.EndEdit()
                UpdateDataTable(kundendatenExt, strCommandExt)
            Catch sqlex As SqlException
                MessageBox.Show(sqlex.Message + " Die Eingaben in einem Feld sind länger als in der Datenbank erlaubt: Fassen Sie sich kürzer oder lassen Sie die Datenbankspalten verbreitern", "SQL-Datenbank-Fehler in Tabelle: " + profil)

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error AlterRowExt")
            Finally
                If connection.State = ConnectionState.Open Then
                    connection.Close()
                End If
            End Try
        End If
    End Sub


    'INSERT
    Private Sub AddRow_Click(sender As System.Object, e As Janus.Windows.Ribbon.CommandEventArgs) Handles btnAddRow.Click
        AddRow()
        AddRowExt()
        RefreshOverview()
    End Sub

    Private Sub AddRow()
        Try
            Dim kundendaten As New DataTable
            kundendaten = RetrieveDataTable(strCommand, tablename)

            Dim typeArray = AnalyseTable_datatype(tablename)



            Dim i = 0
            Dim row As DataRow = kundendaten.NewRow()
            For Each element As Object In columnnames
                If columnnames(i) = "Mandant" Then
                    'row(i) = newMandant
                    Dim cbc = CType(Ribbon1.SelectedTab.Groups.Item("group_DatensatzBearbeiten").Commands("cb_select"), ComboBoxCommand)
                    row(i) = cbc.ComboBox.SelectedItem().Text()

                ElseIf columnnames(i) = "Nummer" Then
                    If typeArray(i) = "int" Then
                        number = CalculateNumber()
                        row(i) = number
                    Else
                        Dim numberString = Dialog()
                        If numberString = "Fehler" Then
                            Exit Sub
                        Else
                            row(i) = numberString
                        End If
                    End If
                ElseIf columnnames(i) = "Profil" Then
                    row(i) = profil
                ElseIf columnnames(i) = "Erstellt" Then
                    row(i) = DateTime.Today
                End If
                i += 1
            Next

            kundendaten.Rows.Add(row)
            InsertDataTable(kundendaten, strCommand)
        Catch ex As InvalidCastException
            MessageBox.Show("Bitte im Feld Nummer eine neue Ganzzahl eingeben", "Error AddRow")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error AddRow")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub AddRowExt()
        If ExtExists Then
            Try

                Dim kundendatenExt As New DataTable
                kundendatenExt = RetrieveDataTable(strCommandExt, tablenameExt)

                Dim dataArray = AnalyseTable_datatype(tablenameExt)
                Dim i = 0
                Dim rowExt As DataRow = kundendatenExt.NewRow()
                For Each element As Object In columnnamesExt
                    If columnnamesExt(i) = "Mandant" Then
                        'rowExt(i) = newMandant
                        Dim cbc = CType(Ribbon1.SelectedTab.Groups.Item("group_DatensatzBearbeiten").Commands("cb_select"), ComboBoxCommand)
                        rowExt(i) = cbc.ComboBox.SelectedItem.Text()

                    ElseIf columnnamesExt(i) = "Nummer" Then
                        If dataArray(i) = "int" Then
                            rowExt(i) = number
                        Else
                            rowExt(i) = number.ToString()
                        End If
                    ElseIf columnnamesExt(i) = "Profil" Then
                        rowExt(i) = profil
                    End If
                    i += 1
                Next

                kundendatenExt.Rows.Add(rowExt)

                InsertDataTable(kundendatenExt, strCommandExt)

            Catch ex As InvalidCastException
                'Wenn hier der Fehler auftritt, dann auch bei der anderen Tabelle vorher schon. Er braucht nur einmal angezeigt werden.
                'Abgebrochen werden muss der vorgang trotzdem
                'MessageBox.Show("Bitte im Feld Nummer nur eine Ganzzahl eingeben", "Error AddRow")
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error AddRowExt")
            Finally
                If connection.State = ConnectionState.Open Then
                    connection.Close()
                End If
            End Try
        End If
    End Sub

    Private Function CalculateNumber()
        Try
            Dim numberCommand = ("SELECT MAX (Nummer) FROM " + fulltablename)
            Dim numbertable As New DataTable
            numbertable = RetrieveDataTable(numberCommand, "MaxNummber")
            number = numbertable.Select().FirstOrDefault().Field(Of Integer)(0)
            number += 1
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error CalculateNumber")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
        Return number
    End Function

    Private Function Dialog()
        Dim numberdialog As New InsertDialog()
        If numberdialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            newnumber = False
            Try
                Dim numberString = numberdialog.Nummer_EditBox.Text
                Dim checkcommand = ("Select Nummer FROM " + fulltablename + " WHERE Nummer = '" + numberString + "'")

                connection.Open()
                Dim selectcommand As SqlCommand
                selectcommand = New SqlCommand(checkcommand, connection)
                selectcommand.CommandTimeout = 3000
                Dim result = selectcommand.ExecuteScalar()
                If IsNothing(result) Then
                    newnumber = True
                    Return numberString
                Else
                    MessageBox.Show("Diese Nummer ist bereits vergeben", "Vorgang abgebrochen")
                    newnumber = False
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error AddRow Dialog")
            Finally
                If connection.State = ConnectionState.Open Then
                    connection.Close()
                End If
            End Try
        Else
            newnumber = False
        End If
        Return "Fehler"
    End Function

    'REFRESH
    Private Sub RefreshOverview()
        Try
            Dim strCommandOverview = "SELECT Mandant, Nummer " + nameOverview + " FROM " + fulltablename
            Dim overview As New DataTable
            overview = RetrieveDataTable(strCommandOverview, "Overview")

            Me.GridEXOverview.SetDataBinding(overview, "")
            Me.GridEXOverview.RetrieveStructure()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error RefreshOverview")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub RefreshMainTable()
        Try
            Dim kundendaten As New DataTable
            kundendaten = RetrieveDataTable(strCommand, tablename)

            Me.GridEXMain.SetDataBinding(kundendaten, "")
            Me.GridEXMain.RetrieveStructure()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error Refresh MainTable")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub RefreshExtTable()
        If ExtExists Then
            Try

                Dim kundendatenExt As New DataTable
                kundendatenExt = RetrieveDataTable(strCommandExt, tablenameExt)

                Me.GridEXExt.SetDataBinding(kundendatenExt, "")
                Me.GridEXExt.RetrieveStructure()

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error Refresh ExtTable")
            Finally
                If connection.State = ConnectionState.Open Then
                    connection.Close()
                End If
            End Try
        Else
            Me.GridEXExt.ClearStructure()
        End If
    End Sub

    'SELECTION CHANGED
    Private Sub GridEXOverview_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles GridEXOverview.SelectionChanged
        If Not IsNothing(GridEXOverview) Then
            If Not IsNothing(GridEXOverview.CurrentRow) Then
                If Not IsNothing(GridEXOverview.CurrentRow.DataRow) Then
                    SelectionChanged()
                    RefreshMainTable()
                    RefreshExtTable()
                End If
            End If
        End If
    End Sub

    Private Sub SelectionChanged()
        Try
            mandant = "'" + GridEXOverview.CurrentRow.Cells("mandant").Value.ToString + "'"
            nummer = "'" + GridEXOverview.CurrentRow.Cells("nummer").Value.ToString() + "'"
            strCommand = ("SELECT " + dynamicColumnNames + " FROM " + fulltablename + " WHERE Mandant = " + mandant + " AND Nummer = " + nummer + " AND Profil = '" + profil + "'")
            strCommandExt = ("SELECT " + dynamicColumnNamesExt + " FROM " + fulltablenameExt + " WHERE Mandant = " + mandant + " AND Nummer = " + nummer + " AND Profil = '" + profil + "'")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error SelectionChanged 1")
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Sub GridEXOverview_CurrentCellChanging(sender As System.Object, e As Janus.Windows.GridEX.CurrentCellChangingEventArgs) Handles GridEXOverview.CurrentCellChanging
        If GridEXOverview.CurrentRow IsNot e.Row And unsavedChanges Then
            Dim result = MessageBox.Show("Speichern?", "Es sind ungepeicherte Änderungen vorhanden", MessageBoxButtons.YesNoCancel)
            If result = Windows.Forms.DialogResult.Cancel Then
                e.Cancel = True
            ElseIf result = Windows.Forms.DialogResult.Yes Then
                AlterRow()
                AlterRowExt()
                unsavedChanges = False
                btnAlterRow.Enabled = False
            ElseIf result = Windows.Forms.DialogResult.No Then
                unsavedChanges = False
                btnAlterRow.Enabled = False
            End If
        End If
    End Sub

    Private Sub GridEXMain_CellEdited(sender As System.Object, e As Janus.Windows.GridEX.ColumnActionEventArgs) Handles GridEXMain.CellEdited
        unsavedChanges = True
        btnAlterRow.Enabled = True
    End Sub

    Private Sub GridEXExt_CellEdited(sender As System.Object, e As Janus.Windows.GridEX.ColumnActionEventArgs) Handles GridEXExt.CellEdited
        unsavedChanges = True
        btnAlterRow.Enabled = True
    End Sub

    'TABLE PROCEDURES
    Private Function RetrieveDataTable(query As String, tablename As String)
        Using adapter As New SqlDataAdapter(query, connection)
            Dim dt As New DataTable(tablename)
            adapter.Fill(dt)
            Return dt
        End Using
    End Function

    Private Sub UpdateDataTable(dt As DataTable, query As String)
        Using adapter As New SqlDataAdapter(query, connection)
            Dim cb As New SqlCommandBuilder(adapter)
            cb.GetUpdateCommand(True)
            cb.ConflictOption = ConflictOption.CompareRowVersion
            adapter.Update(dt)
        End Using
    End Sub

    Private Sub InsertDataTable(dt As DataTable, query As String)
        Using adapter As New SqlDataAdapter(query, connection)
            Dim cb As New SqlCommandBuilder(adapter)
            cb.GetInsertCommand(True)
            cb.ConflictOption = ConflictOption.CompareRowVersion
            adapter.Update(dt)
        End Using
    End Sub

    Private Sub DeleteDataTable(dt As DataTable, query As String)
        Using adapter As New SqlDataAdapter(query, connection)
            Dim cb As New SqlCommandBuilder(adapter)
            cb.GetDeleteCommand(True)
            cb.ConflictOption = ConflictOption.CompareRowVersion
            adapter.Update(dt)
        End Using
    End Sub

    'SIDESTEP BUTTONS
    Private Sub btnEnd_Click(sender As System.Object, e As Janus.Windows.Ribbon.CommandEventArgs) Handles btnEnd.Click
        Dispose()
    End Sub

    Private Sub btnAbout_Click(sender As System.Object, e As Janus.Windows.Ribbon.CommandEventArgs) Handles btnAbout.Click
        frmInfo.ShowDialog()

    End Sub

    'Auslesen der SQL-Datentypen
    Private Function AnalyseTable_datatype(a_tablename As String)
        Dim typeCommand = "SELECT data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name LIKE " + a_tablename
        Dim datatypes As DataTable
        datatypes = RetrieveDataTable(typeCommand, "DataTypes")
        Dim i = 0
        Dim typeArray = New ArrayList
        For Each row In datatypes.Rows
            typeArray.Add(datatypes.Rows.Item(i).Item(0))
            i += 1
        Next
        Return typeArray
    End Function

    'Private Function getTablename(fulltablename As String)
    '    Dim tablename = fulltablename.Substring(4, fulltablename.Length - 4)
    '    MessageBox.Show(fulltablename + " -> " + tablename, "getTablename")

    '    'Const tablenameERPKunde = "'t_ERPKunde'"
    '    'Const tablenameExtERPKunde = "'t_ERPKundeExt'"
    '    'Const fulltablenameERPKunde = "dbo.t_ERPKunde"
    '    'Const fulltablenameExtERPKunde = "dbo.t_ERPKundeExt"
    '    'Const nameOverviewERPKunde = ", Name1"
    '    'Const profilERPKunde = "ERPKunde"
    '    'Const ERPKundeExtExists = True
    '    Return tablename
    'End Function

    Private Function GetTablename(profil As String)
        Dim newtablename As String = ""
        newtablename = "t_" + profil
        Return newtablename
    End Function

    Private Function GetTablenameExt(profil As String)
        Dim newtablenameExt As String = ""
        newtablenameExt = "t_" + profil + "Ext"
        Return newtablenameExt
    End Function

    Private Function GetFullTablename(profil As String)
        Dim newfulltablename As String = ""
        newfulltablename = "dbo.t_" + profil + ""
        Return newfulltablename
    End Function

    Private Function GetFullTablenameExt(profil As String)
        Dim newfulltablenameExt As String = ""
        newfulltablenameExt = "dbo.t_" + profil + "Ext"
        Return newfulltablenameExt
    End Function


    Private Sub Tabledata_Click(sender As System.Object, e As Janus.Windows.Ribbon.CommandEventArgs)
        MessageBox.Show("Tabellenname = " + GetTablename(profil) + vbNewLine + "TabellennameExt = " + GetTablenameExt(profil) + vbNewLine + "Voller Tabellenname = " + GetFullTablename(profil) + vbNewLine + "Voller TabellennameExt = " + GetFullTablenameExt(profil))

    End Sub
    ''dynamsische Ribbon-Erstellung , Das Ribbon im Entwurf is Unsichtbar
    'Private Sub CreateRibbon()
    '    Dim newRibbon As Ribbon = New Ribbon()
    '    For Each tableprofile In TablesTabs
    '        CreateTab(newRibbon, tableprofile)
    '    Next
    '    newRibbon.Show()
    '    newRibbon.Refresh()
    'End Sub
    'dynamische Tab-Erstellung
    Private Sub CreateTab(profil As String)
        Dim newRibbonTab As RibbonTab = New RibbonTab(profil)
        newRibbonTab.Groups.Add(CreateGroupProgramm(newRibbonTab))
        newRibbonTab.Groups.Add(CreateGroupDatensatzBearbeiten(newRibbonTab))

        Ribbon1.Tabs.Add(newRibbonTab)
    End Sub

    Private Function CreateGroupProgramm(tab As RibbonTab)
        Dim newGroupProgramm As RibbonGroup = New RibbonGroup("group_Programm", "Programm")

        Dim btn_Beenden As ButtonCommand = New ButtonCommand("btn_beenden" + tab.Name, "Beenden")
        btn_Beenden.Icon = New Icon("door_32.ico")
        AddHandler btn_Beenden.Click, AddressOf btnEnd_Click
        newGroupProgramm.Commands.Add(btn_Beenden)

        Dim btn_Ueber As ButtonCommand = New ButtonCommand("btn_ueber" + tab.Name, "Über...")
        btn_Ueber.Icon = New Icon("SidestepOhne.ico")
        AddHandler btn_Ueber.Click, AddressOf btnAbout_Click
        newGroupProgramm.Commands.Add(btn_Ueber)

        Return newGroupProgramm
    End Function

    Private Function CreateGroupDatensatzBearbeiten(tab As RibbonTab)
        Dim newGroupDatensatzBearbeiten As RibbonGroup = New RibbonGroup("group_DatensatzBearbeiten", "Datensatz bearbeiten")

        Dim btn_delete As ButtonCommand = New ButtonCommand("btn_delete" + tab.Name, "Aktuelle Zeile löschen")
        btn_delete.Icon = New Icon("spread_sheet_delete_32.ico")
        AddHandler btn_delete.Click, AddressOf DeleteRow_Click
        newGroupDatensatzBearbeiten.Commands.Add(btn_delete)

        Dim btn_update As ButtonCommand = New ButtonCommand("btn_update" + tab.Name, "Aktuelle Zeile speichern")
        btn_update.Icon = New Icon("spread_sheet_save_32.ico")
        AddHandler btn_update.Click, AddressOf AlterRow_Click
        newGroupDatensatzBearbeiten.Commands.Add(btn_update)

        Dim btn_insert As ButtonCommand = New ButtonCommand("btn_insert" + tab.Name, "Neue Zeile hinzufügen")
        btn_insert.Icon = New Icon("spread_sheet_add_32.ico")
        AddHandler btn_insert.Click, AddressOf AddRow_Click
        newGroupDatensatzBearbeiten.Commands.Add(btn_insert)

        Dim cb_select As ComboBoxCommand = New ComboBoxCommand("cb_select" + tab.Name, "Mandant für Neue Zeile auswählen")
        cb_select.ComboBox.Items.AddRange({New UIComboBoxItem("Schlautmann"), New UIComboBoxItem("Immobilien"), New UIComboBoxItem("Privat")})
        newGroupDatensatzBearbeiten.Commands.Add(cb_select)
        Return newGroupDatensatzBearbeiten
    End Function

    Private Sub TabChanging(sender As System.Object, e As Janus.Windows.Ribbon.TabCancelEventArgs) Handles Ribbon1.ChangingSelectedTab
        If unsavedChanges Then
            Dim result = MessageBox.Show("Speichern?", "Es sind ungepeicherte Änderungen vorhanden", MessageBoxButtons.YesNoCancel)
            If result = Windows.Forms.DialogResult.Cancel Then
                e.Cancel = True
                Exit Sub
            ElseIf result = Windows.Forms.DialogResult.Yes Then
                AlterRow()
                AlterRowExt()
                unsavedChanges = False
                btnAlterRow.Enabled = False
            ElseIf result = Windows.Forms.DialogResult.No Then
                unsavedChanges = False
                btnAlterRow.Enabled = False
            End If
        End If
        SelectTable(e.Tab.Text)
    End Sub
End Class
