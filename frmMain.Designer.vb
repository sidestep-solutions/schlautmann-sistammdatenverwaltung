﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Dim JanusColorScheme1 As Janus.Windows.Common.JanusColorScheme = New Janus.Windows.Common.JanusColorScheme()
        Dim GridEXOverview_DesignTimeLayout As Janus.Windows.GridEX.GridEXLayout = New Janus.Windows.GridEX.GridEXLayout()
        Me.Ribbon1 = New Janus.Windows.Ribbon.Ribbon()
        Me.RibbonTab2 = New Janus.Windows.Ribbon.RibbonTab()
        Me.RibbonGroup2 = New Janus.Windows.Ribbon.RibbonGroup()
        Me.btnEnd = New Janus.Windows.Ribbon.ButtonCommand()
        Me.btnAbout = New Janus.Windows.Ribbon.ButtonCommand()
        Me.RibbonGroup1 = New Janus.Windows.Ribbon.RibbonGroup()
        Me.btnDeleteRow = New Janus.Windows.Ribbon.ButtonCommand()
        Me.btnAlterRow = New Janus.Windows.Ribbon.ButtonCommand()
        Me.btnAddRow = New Janus.Windows.Ribbon.ButtonCommand()
        Me.GridEXMain = New Janus.Windows.GridEX.GridEX()
        Me.VisualStyleManager1 = New Janus.Windows.Common.VisualStyleManager(Me.components)
        Me.GridEXOverview = New Janus.Windows.GridEX.GridEX()
        Me.OverviewBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EloSiConnectDataSet = New SiStammdatenverwaltung.eloSiConnectDataSet()
        Me.GridEXExt = New Janus.Windows.GridEX.GridEX()
        CType(Me.Ribbon1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridEXMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridEXOverview, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OverviewBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EloSiConnectDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridEXExt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Ribbon1
        '
        Me.Ribbon1.ControlBoxIcon = CType(resources.GetObject("Ribbon1.ControlBoxIcon"), System.Drawing.Icon)
        Me.Ribbon1.ControlBoxMenu.SuperTipSettings.ImageListProvider = Me.Ribbon1.ControlBoxMenu
        Me.Ribbon1.Location = New System.Drawing.Point(0, 0)
        Me.Ribbon1.Name = "Ribbon1"
        Me.Ribbon1.Office2007ColorScheme = Janus.Windows.Ribbon.Office2007ColorScheme.Silver
        Me.Ribbon1.ShowCustomizeButton = False
        Me.Ribbon1.Size = New System.Drawing.Size(1196, 54)
        '
        '
        '
        Me.Ribbon1.SuperTipComponent.AutoPopDelay = 2000
        Me.Ribbon1.SuperTipComponent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ribbon1.SuperTipComponent.ImageList = Nothing
        Me.Ribbon1.TabIndex = 0
        '
        'RibbonTab2
        '
        Me.RibbonTab2.Key = "RibbonTab2"
        Me.RibbonTab2.Name = "RibbonTab2"
        Me.RibbonTab2.Text = ""
        '
        'RibbonGroup2
        '
        Me.RibbonGroup2.DialogButtonSuperTipSettings.ImageListProvider = Me.RibbonGroup2
        Me.RibbonGroup2.Key = "RibbonGroup2"
        Me.RibbonGroup2.Name = "RibbonGroup2"
        Me.RibbonGroup2.SuperTipSettings.ImageListProvider = Me.RibbonGroup2
        '
        'btnEnd
        '
        Me.btnEnd.Key = "btnEnd"
        Me.btnEnd.Name = "btnEnd"
        Me.btnEnd.SuperTipSettings.ImageListProvider = Me.btnEnd
        Me.btnEnd.Text = ""
        '
        'btnAbout
        '
        Me.btnAbout.Key = "btnAbout"
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.SuperTipSettings.ImageListProvider = Me.btnAbout
        Me.btnAbout.Text = ""
        '
        'RibbonGroup1
        '
        Me.RibbonGroup1.DialogButtonSuperTipSettings.ImageListProvider = Me.RibbonGroup1
        Me.RibbonGroup1.Key = ""
        Me.RibbonGroup1.Name = "RibbonGroup1"
        Me.RibbonGroup1.SuperTipSettings.ImageListProvider = Me.RibbonGroup1
        '
        'btnDeleteRow
        '
        Me.btnDeleteRow.Key = "btnDeleteRow"
        Me.btnDeleteRow.Name = "btnDeleteRow"
        Me.btnDeleteRow.SuperTipSettings.ImageListProvider = Me.btnDeleteRow
        Me.btnDeleteRow.Text = ""
        '
        'btnAlterRow
        '
        Me.btnAlterRow.Key = "btnAlterRow"
        Me.btnAlterRow.Name = "btnAlterRow"
        Me.btnAlterRow.SuperTipSettings.ImageListProvider = Me.btnAlterRow
        Me.btnAlterRow.Text = ""
        '
        'btnAddRow
        '
        Me.btnAddRow.Key = "btnAddRow"
        Me.btnAddRow.Name = "btnAddRow"
        Me.btnAddRow.SuperTipSettings.ImageListProvider = Me.btnAddRow
        Me.btnAddRow.Text = ""
        '
        'GridEXMain
        '
        Me.GridEXMain.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEXMain.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEXMain.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridEXMain.CardWidth = 354
        Me.GridEXMain.CenterSingleCard = False
        Me.GridEXMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridEXMain.Location = New System.Drawing.Point(340, 150)
        Me.GridEXMain.Name = "GridEXMain"
        Me.GridEXMain.Office2007ColorScheme = Janus.Windows.GridEX.Office2007ColorScheme.Silver
        Me.GridEXMain.Size = New System.Drawing.Size(380, 500)
        Me.GridEXMain.TabIndex = 4
        Me.GridEXMain.View = Janus.Windows.GridEX.View.SingleCard
        Me.GridEXMain.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        Me.GridEXMain.VisualStyleManager = Me.VisualStyleManager1
        '
        'VisualStyleManager1
        '
        JanusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText
        JanusColorScheme1.Name = "Scheme0"
        JanusColorScheme1.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Silver
        JanusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty
        JanusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007
        Me.VisualStyleManager1.ColorSchemes.Add(JanusColorScheme1)
        '
        'GridEXOverview
        '
        Me.GridEXOverview.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.[False]
        Me.GridEXOverview.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridEXOverview.DataSource = Me.OverviewBindingSource
        GridEXOverview_DesignTimeLayout.LayoutString = resources.GetString("GridEXOverview_DesignTimeLayout.LayoutString")
        Me.GridEXOverview.DesignTimeLayout = GridEXOverview_DesignTimeLayout
        Me.GridEXOverview.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic
        Me.GridEXOverview.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridEXOverview.GroupByBoxVisible = False
        Me.GridEXOverview.Location = New System.Drawing.Point(10, 150)
        Me.GridEXOverview.Name = "GridEXOverview"
        Me.GridEXOverview.Office2007ColorScheme = Janus.Windows.GridEX.Office2007ColorScheme.Silver
        Me.GridEXOverview.Size = New System.Drawing.Size(325, 500)
        Me.GridEXOverview.TabIndex = 5
        Me.GridEXOverview.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        Me.GridEXOverview.VisualStyleManager = Me.VisualStyleManager1
        '
        'OverviewBindingSource
        '
        Me.OverviewBindingSource.DataMember = "overview"
        Me.OverviewBindingSource.DataSource = Me.EloSiConnectDataSet
        '
        'EloSiConnectDataSet
        '
        Me.EloSiConnectDataSet.DataSetName = "eloSiConnectDataSet"
        Me.EloSiConnectDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridEXExt
        '
        Me.GridEXExt.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEXExt.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.[True]
        Me.GridEXExt.CardWidth = 354
        Me.GridEXExt.CenterSingleCard = False
        Me.GridEXExt.Location = New System.Drawing.Point(730, 150)
        Me.GridEXExt.Name = "GridEXExt"
        Me.GridEXExt.Office2007ColorScheme = Janus.Windows.GridEX.Office2007ColorScheme.Silver
        Me.GridEXExt.Size = New System.Drawing.Size(380, 460)
        Me.GridEXExt.TabIndex = 6
        Me.GridEXExt.View = Janus.Windows.GridEX.View.SingleCard
        Me.GridEXExt.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007
        Me.GridEXExt.VisualStyleManager = Me.VisualStyleManager1
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1196, 681)
        Me.Controls.Add(Me.GridEXExt)
        Me.Controls.Add(Me.GridEXOverview)
        Me.Controls.Add(Me.GridEXMain)
        Me.Controls.Add(Me.Ribbon1)
        Me.Name = "frmMain"
        Me.Text = "Form1"
        CType(Me.Ribbon1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridEXMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridEXOverview, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OverviewBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EloSiConnectDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridEXExt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Ribbon1 As Janus.Windows.Ribbon.Ribbon
    Friend WithEvents RibbonTab2 As Janus.Windows.Ribbon.RibbonTab
    Friend WithEvents GridEXMain As Janus.Windows.GridEX.GridEX
    Friend WithEvents EloSiConnectDataSet As SiStammdatenverwaltung.eloSiConnectDataSet
    Friend WithEvents GridEXOverview As Janus.Windows.GridEX.GridEX
    Friend WithEvents OverviewBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridEXExt As Janus.Windows.GridEX.GridEX
    Friend WithEvents RibbonGroup1 As Janus.Windows.Ribbon.RibbonGroup
    Friend WithEvents btnDeleteRow As Janus.Windows.Ribbon.ButtonCommand
    Friend WithEvents btnAlterRow As Janus.Windows.Ribbon.ButtonCommand
    Friend WithEvents btnAddRow As Janus.Windows.Ribbon.ButtonCommand
    Friend WithEvents VisualStyleManager1 As Janus.Windows.Common.VisualStyleManager
    Friend WithEvents RibbonGroup2 As Janus.Windows.Ribbon.RibbonGroup
    Friend WithEvents btnEnd As Janus.Windows.Ribbon.ButtonCommand
    Friend WithEvents btnAbout As Janus.Windows.Ribbon.ButtonCommand

End Class
